#include <iostream>
using namespace std;

//������� ����� Animal � ��������� ������� Voice(), ������� ������� � ������� ������ � �������
class Animal
{
public:
	virtual void Voice() {}
	~Animal();
};

/*����������� �� Animal ������� 3 ������, � � ��� ����������� ����� Voice()
����� �������, ����� ��� ������� � ������ Dog ������� � ������� Woof*/
class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow\n";
	}
};

class Chicken : public Animal
{
public:
	void Voice() override
	{
		cout << "Koko\n";
	}
};

/*� ������� main ������� ������ ���������� ���� Animal � ���������
���� ������ ��������� ��������� �������*/
int main()
{
	Animal* animals[3];
	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Chicken();
	
//�������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice()

	for (Animal* a : animals)
	{
		a->Voice();
		delete a;
	}
	return 0;
}